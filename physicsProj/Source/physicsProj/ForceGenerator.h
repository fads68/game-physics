#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

static class PHYSICSPROJ_API ForceGenerator
{
public:
	ForceGenerator();
	~ForceGenerator();

	static ForceGenerator* GetInstance();


	// f_gravity = mg
	FVector2D GenerateForce_gravity(FVector2D worldUp_unit, float gravity, float particleMass);
	
	// f_normal = proj(f_gravity, surfaceNormal_unit)
	FVector2D GenerateForce_normal(FVector2D f_gravity, FVector2D surfaceNormal_unit);
	
	// f_sliding = f_gravity + f_normal
	FVector2D GenerateForce_sliding(FVector2D f_gravity, FVector2D f_normal);
	
	// f_friction_s = -f_opposing if less than max, else -coeff*f_normal
	FVector2D GenerateForce_friction_static(FVector2D f_normal, FVector2D f_opposing, float frictionCoefficient_static);
	
	// f_friction_k = -coeff*|f_normal| * unit(vel)
	FVector2D GenerateForce_friction_kinetic(FVector2D f_normal, FVector2D particleVelocity, float frictionCoefficient_kinetic);
	
	// f_drag = (p * u^2 * area * coeff)/2
	FVector2D GenerateForce_drag(FVector2D particleVelocity, FVector2D fluidVelocity, float fluidDensity, float objectArea_crossSection, float objectDragCoefficient);
	
	// f_spring = -coeff*(spring length - spring resting length)
	FVector2D GenerateForce_spring(FVector2D particlePosition, FVector2D anchorPosition, float springRestingLength, float springStiffnessCoefficient);

	float gravity;
private:
	static ForceGenerator* instance;
};
