// Fill out your copyright notice in the Description page of Project Settings.


#include "ForceGenerator.h"

ForceGenerator* ForceGenerator::instance = NULL;
ForceGenerator::ForceGenerator()
{
	

}

ForceGenerator::~ForceGenerator()
{

}

ForceGenerator* ForceGenerator::GetInstance()
{
	if (instance != NULL)
	{
		return instance;
	}
	else
	{
		instance = new ForceGenerator();
	}
	return instance;
}

FVector2D ForceGenerator::GenerateForce_gravity(FVector2D worldUp_unit, float gravityConstant, float particleMass)
{
	return particleMass * gravityConstant * worldUp_unit;
}

FVector2D ForceGenerator::GenerateForce_normal(FVector2D f_gravity, FVector2D surfaceNormal_unit)
{
	FVector2D normal = -(FVector2D::DotProduct(f_gravity, surfaceNormal_unit) / FVector2D::DotProduct(surfaceNormal_unit, surfaceNormal_unit)) * surfaceNormal_unit;
	
	return normal;
}

FVector2D ForceGenerator::GenerateForce_sliding(FVector2D f_gravity, FVector2D f_normal)
{
	return f_gravity + f_normal;
}


// f_friction_s = -f_opposing if less than max, else -coeff*f_normal (max amount is coeff*|f_normal|)
FVector2D ForceGenerator::GenerateForce_friction_static(FVector2D f_normal, FVector2D f_opposing, float frictionCoefficient_static)
{
	float overcomeAmount = frictionCoefficient_static * f_normal.Size();

	if (f_opposing.Size() < overcomeAmount)
	{
		return -f_opposing;
	}
	else
	{
		return -f_opposing.Normalize() * overcomeAmount;
	}

}

FVector2D ForceGenerator::GenerateForce_friction_kinetic(FVector2D f_normal, FVector2D particleVelocity, float frictionCoefficient_kinetic)
{
	particleVelocity.Normalize();
	return -frictionCoefficient_kinetic * f_normal.Size() * particleVelocity;
}

FVector2D ForceGenerator::GenerateForce_drag(FVector2D particleVelocity, FVector2D fluidVelocity, float fluidDensity, float objectArea_crossSection, float objectDragCoefficient)
{
	FVector2D relativeVel = fluidVelocity - particleVelocity;
	FVector2D relativeVelSquared = relativeVel * relativeVel.Size();
	
	return fluidDensity * relativeVelSquared * objectArea_crossSection * objectDragCoefficient * 0.5f;
}

FVector2D ForceGenerator::GenerateForce_spring(FVector2D particlePosition, FVector2D anchorPosition, float springRestingLength, float springStiffnessCoefficient)
{
	FVector2D diff = particlePosition - anchorPosition;
	float length = diff.Size();
	FVector2D force = springStiffnessCoefficient * (springRestingLength - length) * diff / length;
	return force;
}

