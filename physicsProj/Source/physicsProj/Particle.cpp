// Fill out your copyright notice in the Description page of Project Settings.


#include "Particle.h"
#include "Engine/EngineCustomTimeStep.h"

// Sets default values
AParticle::AParticle()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mPosition = FVector2D(GetActorLocation());
	mVelocity = FVector2D(1, 0);
	mAcceleration = FVector2D(0, 0);
	time = 0;
	mRotation = 0;
	mAngularAcceleration = 0;
	mAngularVelocity = 1;
	mAggregateForce = FVector2D(0, 0);
}

// Called when the game starts or when spawned
void AParticle::BeginPlay()
{
	Super::BeginPlay();
	SetActorLocation(FVector(mPosition.X, 0, mPosition.Y));
	SetMass(mPublicMass);
	mForceInstance = ForceGenerator::GetInstance();
}

// Called every frame
void AParticle::Tick(float DeltaTime)
{
	//update running time count
	time += DeltaTime;

	//accelerations - currently 2nd derivative of sin for circular path movements		
	FVector2D anchor = FVector2D(3, -3);

	FVector2D worldUp = FVector2D(0, 1);
	FVector2D testNrm = FVector2D(0, 1);

	//apply forces basedd on 
	FVector2D grav = (mForceInstance->GenerateForce_gravity(worldUp, -9.810f, mMass)); //gravity
	if (applyGravity)
	{
		AddForce(grav);
	}

	FVector2D normal = (mForceInstance->GenerateForce_normal(grav, testNrm)); //normal
	if (applyNormal)
	{
		AddForce(normal);
	}
	
	if (applySliding)
	{
		AddForce(mForceInstance->GenerateForce_sliding(normal, grav)); //sliding
	}
	
	if (applyFrictionStatic)
	{
		AddForce(mForceInstance->GenerateForce_friction_static(normal, mVelocity, 0.3f));//static friction
	}

	if (applyFrictionKinetic)
	{
		AddForce(mForceInstance->GenerateForce_friction_kinetic(normal, mVelocity, 0.3f));//kinetic friction;
	}

	if (applyDrag)
	{
		AddForce(mForceInstance->GenerateForce_drag(mVelocity, mDragDirection, 1.125f, 1, 1.05)); //drag force
	}
	
	if (applySpring)
	{
	AddForce(mForceInstance->GenerateForce_spring(mPosition, anchor, 3, 5.33f)); //spring force
	}

	//call update functions
	switch (integrationType)
	{
		case IntegrationTypes::EULER_EXPLICIT:
		{
			UpdatePositionEulerExplicit(DeltaTime);
			UpdateRotationEulerExplicit(DeltaTime); 
			break;
		}
		case IntegrationTypes::KINEMATIC:
		{
			UpdatePositionKinematic(DeltaTime);
			UpdateRotationKinematic(DeltaTime); 
			break;
		}
	
		default:
			break;
	}
	
	UpdateAcceleration();

	//update positions
	SetActorLocation(FVector(mPosition.X, 0, mPosition.Y) * mPositionScale);
	SetActorRotation(FRotator(mRotation, 0, 0) * mRotationScale);

	
	Super::Tick(DeltaTime);

}



void AParticle::UpdatePositionEulerExplicit(float dt)
{
	//euler explicit integration
	mPosition += mVelocity * dt;
	mVelocity += mAcceleration * dt;
}

void AParticle::UpdatePositionKinematic(float dt)
{
	//kinematic integration
	mPosition += mVelocity * dt + mAcceleration * dt * dt * .5;
	mVelocity += mAcceleration * dt;


}

void AParticle::UpdateRotationEulerExplicit(float dt)
{
	//euler explicit integration
	mRotation += mAngularVelocity * dt;
	mAngularVelocity += mAngularAcceleration * dt;

	mRotation = FMath::Fmod(mRotation, 2 * PI);

}

void AParticle::UpdateRotationKinematic(float dt)
{
	mRotation += mAngularVelocity * dt + mAngularAcceleration * dt * dt * 0.5;
	mAngularVelocity += mAngularAcceleration * dt;

	mRotation = FMath::Fmod(mRotation, 2 * PI);

}

void AParticle::UpdateAcceleration()
{
	mAcceleration = mAggregateForce * mMassInverse;

	mAngularAcceleration = mAggregateTorque.X * mInertia;
	mAggregateForce = FVector2D(0, 0);
}

void AParticle::AddForce(FVector2D force)
{
	mAggregateForce += force;
}

void AParticle::SetMass(float newMass)
{
	//mass is the max of 0f and newMass
	mMass = newMass > 0.0f ? newMass : 0.0f;

	mMassInverse = mMass > 0.0f ? 1.0f / mMass : 0.0f;
}

void AParticle::AddForceAtPos(FVector2D force, FVector2D pos)
{
	mAggregateForce += force;
	mAggregateTorque = (pos - (mPosition + mCenterOfMassOffset)) * force;
}
