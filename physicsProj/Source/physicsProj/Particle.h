// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ForceGenerator.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particle.generated.h"


UENUM()
namespace IntegrationTypes {
	enum Type {
		KINEMATIC,
		EULER_EXPLICIT
	};
}

UCLASS()
class PHYSICSPROJ_API AParticle : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AParticle();
	void AddForce(FVector2D force);
	void AddForceAtPos(FVector2D force, FVector2D pos);
	void SetMass(float newMass);
	float GetMass() { return mMass; };
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void UpdatePositionEulerExplicit(float dt);
	void UpdatePositionKinematic(float dt);
	void UpdateRotationEulerExplicit(float dt);
	void UpdateRotationKinematic(float dt);
	void UpdateAcceleration();

	UPROPERTY(EditAnywhere)
	FVector2D mCenterOfMassOffset;


	UPROPERTY(EditAnywhere)
	FVector2D mPosition;
	
	UPROPERTY(EditAnywhere)
	FVector2D mVelocity;

	UPROPERTY(EditAnywhere)
	FVector2D mDragDirection;

	UPROPERTY(EditAnywhere)
	float mPublicMass;
	
	float mMass, mMassInverse, time, mAngularAcceleration, mInertia, mInverseInertia;
	
	FVector2D mAggregateForce, mAcceleration;
	FVector2D mAggregateTorque;

	UPROPERTY(EditAnywhere)
	float mRotation;
	
	UPROPERTY(EditAnywhere)
	float mAngularVelocity;

	UPROPERTY(EditAnywhere)
	float mPositionScale = 100;

	UPROPERTY(EditAnywhere)
	float mRotationScale = 360;


	UPROPERTY(EditAnywhere)
	TEnumAsByte<IntegrationTypes::Type> integrationType;


	UPROPERTY(EditAnywhere)
	bool applyGravity;

	UPROPERTY(EditAnywhere)
	bool applyNormal;

	UPROPERTY(EditAnywhere)
	bool applySliding;

	UPROPERTY(EditAnywhere)
	bool applyFrictionStatic;

	UPROPERTY(EditAnywhere)
	bool applyFrictionKinetic;

	UPROPERTY(EditAnywhere)
	bool applyDrag;

	UPROPERTY(EditAnywhere)
	bool applySpring;
	ForceGenerator* mForceInstance;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};